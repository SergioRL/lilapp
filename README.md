# **Lilapp**

# Documentation
Angular application for consulting and Requesting data from the API [Lilapp-REST](https://gitlab.com/SergioRL/lilapp-rest).

## Components and features
* Toolbar
    * 'Home' button to 'User List'

* User List
    * Paginated list of all users in application. Selecting one of them takes you to the corresponding  User Profile.

* User Profile
    * Detailed info from the user. Include external link to User's website if exists.
    * Paginated list of the User's posts. Clicking one of them takes you to Post Detail.

* Post Detail
    * Details of the Post's title and body.
    * Modification of a Post title. Contains validators for requirement.
    * Modification of a Post body. Contains validators for requirement.
    * Paginated list of the Post's Comments.

## Pipes
* ExternalLinkPipe -> Convert an internal link to an external link. Needed for Users' websites links.
* EscapeNewLinePipe -> Replace all '\n' in text to html new line tag (<br/>). Used for Posts' body correct visualization.

## Possible improvements
* Include login options based on BackendAPI.
* Limitate access to information and actions based on loged user.
* Testing




# Angular Info 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.10.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
