export const environment = {
  production: true,
  api: {
    host: 'http://localhost:8090',
    path: 'lilapp-rest'
  }
};
