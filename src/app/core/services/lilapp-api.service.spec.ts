import { TestBed } from '@angular/core/testing';

import { LilappApiService } from './lilapp-api.service';

describe('LilappApiService', () => {
  let service: LilappApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LilappApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
