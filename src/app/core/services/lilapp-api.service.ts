import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { UserBasicInfo} from '../../features/interfaces/interfaces';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LilappApiService {

  constructor(private http: HttpClient) { }

  public getAllUsersPaginated (request:any):Observable<any> {
    const endpoint = environment.api.host + "/" + environment.api.path + "/user/all/paginated";
    const params = request;
    return this.http.get(endpoint, { params });
  }

  public getUserById (userId:number):Observable<any> {
    const endpoint = environment.api.host + "/" + environment.api.path + "/user/"+userId;
    return this.http.get(endpoint);
  }

  public getPostsByUserId (userId:number, request:any):Observable<any> {
    const endpoint = environment.api.host + "/" + environment.api.path + "/post/user-id/"+userId+"/paginated";
    const params = request;
    return this.http.get(endpoint,  { params });
  }

  public getPostById (postId:number):Observable<any> {
    const endpoint = environment.api.host + "/" + environment.api.path + "/post/"+postId;
    return this.http.get(endpoint);
  }

  public patchPostTitle (postId:number, request:any):Observable<any> {
    const endpoint = environment.api.host + "/" + environment.api.path + "/post/"+postId+"/title";
    const body = request;
    return this.http.patch(endpoint, body);
  }

  public patchPostBody (postId:number, request:any):Observable<any> {
    const endpoint = environment.api.host + "/" + environment.api.path + "/post/"+postId+"/body";
    const body = request;
    return this.http.patch(endpoint, body);
  }

  public getCommentsByPostId (postId:number, request:any):Observable<any> {
    const endpoint = environment.api.host + "/" + environment.api.path + "/comment/post-id/"+postId+"/paginated";
    const params = request;
    return this.http.get(endpoint,  { params });
  }
}
