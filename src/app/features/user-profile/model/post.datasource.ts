import { DataSource } from '@angular/cdk/table';
import { PostInfoListResponse, PostInfo } from '../../interfaces/interfaces';
import { CollectionViewer } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { LilappApiService } from '../../../core/services/lilapp-api.service';
 
export class PostDataSource implements DataSource<PostInfo>{
 
    private postSubject = new BehaviorSubject<PostInfo[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    private countSubject = new BehaviorSubject<number>(0);
    public counter$ = this.countSubject.asObservable();
    
    private userId!:number;
 
    constructor(
        private apiService: LilappApiService,
        userId:number
        ) { 
            this.userId = userId;
         }
 
    connect(collectionViewer: CollectionViewer): Observable<PostInfo[]> {
        return this.postSubject.asObservable();
    }
 
    disconnect(collectionViewer: CollectionViewer): void {
        this.postSubject.complete();
        this.loadingSubject.complete();
        this.countSubject.complete();
    }
 
    loadPosts(pageNumber = 0, pageSize = 5) {
        this.loadingSubject.next(true);


        if (this.userId) {
            console.log(this.userId);

            this.apiService.getPostsByUserId(this.userId, { page: pageNumber, size: pageSize })
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe((result: PostInfoListResponse) => {
                this.postSubject.next(result.content);
                this.countSubject.next(result.totalElements);
            }
            );
            
        } else {
            console.log("@userId is null. Data can't be loaded.")
        }
        
    }
 
}