import { Component, OnInit, ViewChild } from '@angular/core';
import { LilappApiService } from '../../core/services/lilapp-api.service'
import { UserInfo } from '../interfaces/interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { PostDataSource } from './model/post.datasource';
import { tap } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  userId!:number;
  userInfo!: UserInfo;

  displayedColumns = ['title'];
  datasource!: PostDataSource;

  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: LilappApiService) { }

  ngOnInit(): void {
    this.userId =  Number(this.route.snapshot.paramMap.get('userId'))

    this.datasource = new PostDataSource(this.apiService, this.userId);
    this.datasource.loadPosts();

    this.apiService.getUserById(this.userId).subscribe(
      userResponse => {
        this.userInfo = userResponse;
      }
    );
  
  }

  ngAfterViewInit() {

    this.datasource.counter$
      .pipe(
        tap((count) => {
          this.paginator.length = count;
        })
      )
      .subscribe();
 
    this.paginator.page
      .pipe(
        tap(() => this.loadPosts())
      )
      .subscribe();
      
  }

  loadPosts() {
    this.datasource.loadPosts(this.paginator.pageIndex, this.paginator.pageSize);
  }

  postSelected(row:any) {
    this.router.navigateByUrl('/post-detail/'+row.id);
  }

}
