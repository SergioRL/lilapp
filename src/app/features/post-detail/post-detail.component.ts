import { Component, OnInit, ViewChild } from '@angular/core';
import { LilappApiService } from '../../core/services/lilapp-api.service'
import { PostInfo, CommentInfo } from '../interfaces/interfaces';
import { ActivatedRoute } from '@angular/router';
import { CommentDataSource } from './model/comment.datasource';
import { tap } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  postId!:number;
  postInfo!:PostInfo;

  editingTitle:boolean = false;
  disabledTitle: boolean = false;
  editingBody:boolean = false;
  disabledBody: boolean = false;

  titleFormControl:FormControl = new FormControl('', [Validators.required]);
  bodyFormControl:FormControl = new FormControl('', [Validators.required]);

  displayedColumns = ['email', 'name', 'body'];
  datasource!:CommentDataSource;

  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private apiService: LilappApiService) { }

  ngOnInit(): void {
    this.postId =  Number(this.route.snapshot.paramMap.get('postId'))

    this.datasource = new CommentDataSource(this.apiService, this.postId);
    this.datasource.loadComments();

    this.apiService.getPostById(this.postId).subscribe(
      postResponse => {
        this.postInfo = postResponse;

        this.titleFormControl.setValue(this.postInfo.title);
        this.bodyFormControl.setValue(this.postInfo.body);
      }
    );
  
  }

  ngAfterViewInit() {

    this.datasource.counter$
      .pipe(
        tap((count) => {
          this.paginator.length = count;
        })
      )
      .subscribe();
 
    this.paginator.page
      .pipe(
        tap(() => this.loadComments())
      )
      .subscribe();
      
  }

  loadComments() {
    this.datasource.loadComments(this.paginator.pageIndex, this.paginator.pageSize);
  }

  startTitleEditing() {
    this.titleFormControl.setValue(this.postInfo.title);
    this.editingTitle = true;
  }

  saveTitleEditing() {

    this.disabledTitle = true;

    this.apiService.patchPostTitle(
      this.postId,
      {
        title: this.titleFormControl.value
      }).subscribe(
        postResponse => {
          this.postInfo = postResponse;
  
          this.titleFormControl.setValue(this.postInfo.title);
          this.editingTitle = false;
          this.disabledTitle = false;
        },
        error => {
          this.disabledTitle = false;
        }
      );

  }

  cancelTitleEditing() {
    this.editingTitle = false;
  }

  startBodyEditing() {
    this.bodyFormControl.setValue(this.postInfo.body);
    this.editingBody = true;
  }

  saveBodyEditing() {

    this.disabledBody = true;

    this.apiService.patchPostBody(
      this.postId,
      {
        body: this.bodyFormControl.value
      }).subscribe(
        postResponse => {
          this.postInfo = postResponse;
  
          this.bodyFormControl.setValue(this.postInfo.body);
          this.editingBody = false;
          this.disabledBody = false;
        },
        error => {
          this.disabledBody = false;
        }
      );

  }

  cancelBodyEditing() {
    this.editingBody = false;
  }

}
