import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostDetailComponent } from './post-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    PostDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class PostDetailModule { }
