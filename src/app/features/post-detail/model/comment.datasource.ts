import { DataSource } from '@angular/cdk/table';
import { CommentInfoListResponse, CommentInfo } from '../../interfaces/interfaces';
import { CollectionViewer } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { LilappApiService } from '../../../core/services/lilapp-api.service';
 
export class CommentDataSource implements DataSource<CommentInfo>{
 
    private commentSubject = new BehaviorSubject<CommentInfo[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    private countSubject = new BehaviorSubject<number>(0);
    public counter$ = this.countSubject.asObservable();
    
    private postId!:number;
 
    constructor(
        private apiService: LilappApiService,
        postId:number
        ) { 
            this.postId = postId;
         }
 
    connect(collectionViewer: CollectionViewer): Observable<CommentInfo[]> {
        return this.commentSubject.asObservable();
    }
 
    disconnect(collectionViewer: CollectionViewer): void {
        this.commentSubject.complete();
        this.loadingSubject.complete();
        this.countSubject.complete();
    }
 
    loadComments(pageNumber = 0, pageSize = 5) {
        this.loadingSubject.next(true);


        if (this.postId) {
            console.log(this.postId);

            this.apiService.getCommentsByPostId(this.postId, { page: pageNumber, size: pageSize })
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe((result: CommentInfoListResponse) => {
                this.commentSubject.next(result.content);
                this.countSubject.next(result.totalElements);
            }
            );

            console.log(this.commentSubject.getValue());
            
        } else {
            console.log("@postId is null. Data can't be loaded.")
        }
        
    }
 
}