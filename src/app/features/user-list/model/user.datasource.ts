import { DataSource } from '@angular/cdk/table';
import { UserBasicInfoListResponse, UserBasicInfo } from '../../interfaces/interfaces';
import { CollectionViewer } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { LilappApiService } from '../../../core/services/lilapp-api.service';
 
export class UserDataSource implements DataSource<UserBasicInfo>{
 
    private userSubject = new BehaviorSubject<UserBasicInfo[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    private countSubject = new BehaviorSubject<number>(0);
    public counter$ = this.countSubject.asObservable();
 
    constructor(private apiService: LilappApiService) { }
 
    connect(collectionViewer: CollectionViewer): Observable<UserBasicInfo[]> {
        return this.userSubject.asObservable();
    }
 
    disconnect(collectionViewer: CollectionViewer): void {
        this.userSubject.complete();
        this.loadingSubject.complete();
        this.countSubject.complete();
    }
 
    loadUsers(pageNumber = 0, pageSize = 5) {
        this.loadingSubject.next(true);
        this.apiService.getAllUsersPaginated({ page: pageNumber, size: pageSize })
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
            .subscribe((result: UserBasicInfoListResponse) => {
                this.userSubject.next(result.content);
                this.countSubject.next(result.totalElements);
            }
            );
    }
 
}