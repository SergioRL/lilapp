import { Component, OnInit, ViewChild, ChangeDetectorRef  } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import { LilappApiService } from '../../core/services/lilapp-api.service';
import { UserDataSource } from './model/user.datasource';
import { tap } from 'rxjs/operators';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  displayedColumns = ['username', 'email', 'phone'];
  datasource!: UserDataSource;

  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;


  constructor(
    private router: Router,
    private apiService: LilappApiService
    ) { }
 
  ngOnInit() {
    this.datasource = new UserDataSource(this.apiService);
    this.datasource.loadUsers();
  }
 
  ngAfterViewInit() {
    this.datasource.counter$
      .pipe(
        tap((count) => {
          this.paginator.length = count;
        })
      )
      .subscribe();
 
    this.paginator.page
      .pipe(
        tap(() => this.loadUsers())
      )
      .subscribe();
  }
 
  loadUsers() {
    this.datasource.loadUsers(this.paginator.pageIndex, this.paginator.pageSize);
  }
  
  userSelected(row:any) {
    this.router.navigateByUrl('/user-profile/'+row.id);
  }

}

