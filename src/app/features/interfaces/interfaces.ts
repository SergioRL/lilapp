
export interface UserBasicInfo {
    id: number;
    username: string;
    email: string;
    phone: string;
}

export interface UserInfo extends UserBasicInfo{
    city: string;
    website: string;
}

export interface UserBasicInfoListResponse {
    content: UserBasicInfo[];
    totalElements: number;
}

export interface PostInfo {
    id: number;
    userId: number;
    title: string;
    body: string;
}

export interface PostInfoListResponse {
    content: PostInfo[];
    totalElements: number;
}

export interface CommentInfo {
    id: number;
    postId: number;
    email: string;
    nombre: string;
    body: string;
}

export interface CommentInfoListResponse {
    content: CommentInfo[];
    totalElements: number;
}