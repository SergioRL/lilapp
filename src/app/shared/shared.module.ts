import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { ExternalLinkPipe } from './pipes/external-link.pipe';
import { EscapeNewLinePipe } from './pipes/escape-new-line.pipe';



@NgModule({
  declarations: [
    ExternalLinkPipe,
    EscapeNewLinePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatTableModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    ExternalLinkPipe,
    EscapeNewLinePipe
  ]
})
export class SharedModule { }
