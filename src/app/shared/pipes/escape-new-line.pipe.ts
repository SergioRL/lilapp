import { Pipe, PipeTransform } from '@angular/core';

/*
 * Adds http:// at the begining if does not start
 * with it. Look for https:// too.
*/
@Pipe({name: 'escapeNewLine'})
export class EscapeNewLinePipe implements PipeTransform {
  transform(value: string): string {
    return value.split("\n").join("<br/>");
  }
}