import { Pipe, PipeTransform } from '@angular/core';

/*
 * Adds http:// at the begining if does not start
 * with it. Look for https:// too.
*/
@Pipe({name: 'externalLink'})
export class ExternalLinkPipe implements PipeTransform {
  transform(value: string): string {
    let add = value && !value.match('https?://.*');
    return add ? 'http://'+value : value;
  }
}