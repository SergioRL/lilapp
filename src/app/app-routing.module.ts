import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostDetailComponent } from './features/post-detail/post-detail.component';
import { UserListComponent } from './features/user-list/user-list.component';
import { UserProfileComponent } from './features/user-profile/user-profile.component';

const routes: Routes = [
  {path: 'user-list', component: UserListComponent},
  {path: 'user-profile/:userId', component: UserProfileComponent},
  {path: 'post-detail/:postId', component: PostDetailComponent},
  { path: '',   redirectTo: '/user-list', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
